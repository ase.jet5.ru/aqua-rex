<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use Jet5\Api;
$api = new Api();
echo json_encode($api->exec());
die();