<?php
namespace Jet5\IBlock;

class ContentMenu extends ContentType
{
    const IBLOCK_CODE = 'CONTENT_MENU';

    const IBLOCK_STRUCTURE = [
        "NAME"                  => "Меню",
        "CODE"                  => self::IBLOCK_CODE,
        "VERSION"               => 1,
        "LID"                   => self::SITE_LID,
        "ACTIVE"                => "Y",
        "SORT"                  => 50,
        "LIST_PAGE_URL"         => "",
        "SECTION_PAGE_URL"      => "",
        "DETAIL_PAGE_URL"       => "",
        "CANONICAL_PAGE_URL"    => "",
        "EDIT_FILE_BEFORE"      => ""
    ];

    const IBLOCK_FIELDS = [];

    const IBLOCK_PROPERTIES = [];

}