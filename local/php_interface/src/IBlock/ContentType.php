<?php
namespace Jet5\IBlock;

abstract class ContentType
{
    const SITE_LID = ['s1'];
    const IBLOCK_TYPE = 'CONTENT';
    const IBLOCK_TYPE_STRUCTURE = [
        'ID'        => self::IBLOCK_TYPE,
        'SECTIONS'  => 'Y',
        'IN_RSS'    => 'N',
        'SORT'      => 10,
        'LANG'      => [
            'ru' => [
                'NAME'          => 'Контент',
                'SECTION_NAME'  => 'Разделы',
                'ELEMENT_NAME'  => 'Элементы',
            ],
            'en' => [
                'NAME'          => 'Content',
                'SECTION_NAME'  => 'Sections',
                'ELEMENT_NAME'  => 'Elements',
            ],
        ]
    ];

    public function __construct() {
    }
}