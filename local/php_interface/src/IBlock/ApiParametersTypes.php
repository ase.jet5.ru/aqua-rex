<?php
namespace Jet5\IBlock;

class ApiParametersTypes extends ApiType
{
    const IBLOCK_CODE = 'API_PARAMETRS_TYPES';

    const IBLOCK_STRUCTURE = [
        "NAME"                  => "Типы параметров",
        "CODE"                  => self::IBLOCK_CODE,
        "VERSION"               => 1,
        "LID"                   => self::SITE_LID,
        "ACTIVE"                => "Y",
        "SORT"                  => 30,
        "LIST_PAGE_URL"         => "",
        "SECTION_PAGE_URL"      => "",
        "DETAIL_PAGE_URL"       => "",
        "CANONICAL_PAGE_URL"    => "",
        "EDIT_FILE_BEFORE"      => ""
    ];

    const IBLOCK_FIELDS = [
        "CODE" => [
            "NAME" => "Символьный код",
            "IS_REQUIRED" => "Y",
            "DEFAULT_VALUE" => [
                "UNIQUE" => "Y",
                "TRANSLITERATION" => "Y",
                "TRANS_LEN" => 255,
                "TRANS_CASE" => "L",
                "TRANS_SPACE" => "-",
                "TRANS_OTHER" => "-",
                "TRANS_EAT" => "Y",
                "USE_GOOGLE" => "N"
            ]
        ],
    ];

    const IBLOCK_PROPERTIES = [
        [
            "CODE"          => "ARR_SEPARATOR",
            "NAME"          => "Разделитель",
            "ACTIVE"        => "Y",
            "SORT"          => 10,
            "PROPERTY_TYPE" => "S",
            "MULTIPLE"      => "N",
            "ROW_COUNT"     => 1,
            "COL_COUNT"     => 5,
            "IS_REQUIRED"   => "N",
            "DEFAULT_VALUE" => ",",
        ],
    ];
}