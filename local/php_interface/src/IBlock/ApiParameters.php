<?php
namespace Jet5\IBlock;

class ApiParameters extends ApiType
{
    const IBLOCK_CODE = 'API_PARAMETRS';

    const IBLOCK_STRUCTURE = [
        "NAME"                  => "Параметры методов",
        "CODE"                  => self::IBLOCK_CODE,
        "VERSION"               => 1,
        "LID"                   => self::SITE_LID,
        "ACTIVE"                => "Y",
        "SORT"                  => 20,
        "LIST_PAGE_URL"         => "",
        "SECTION_PAGE_URL"      => "",
        "DETAIL_PAGE_URL"       => "",
        "CANONICAL_PAGE_URL"    => "",
        "EDIT_FILE_BEFORE"      => ""
    ];

    const IBLOCK_FIELDS = [
        "CODE" => [
            "NAME" => "Символьный код",
            "IS_REQUIRED" => "Y",
            "DEFAULT_VALUE" => [
                "UNIQUE" => "N",
                "TRANSLITERATION" => "Y",
                "TRANS_LEN" => 255,
                "TRANS_CASE" => "L",
                "TRANS_SPACE" => "-",
                "TRANS_OTHER" => "-",
                "TRANS_EAT" => "Y",
                "USE_GOOGLE" => "N"
            ]
        ],
    ];

    const IBLOCK_PROPERTIES = [
        [
            "CODE"                  => "IS_REQUIRED",
            "NAME"                  => "Обязательный",
            "ACTIVE"                => "Y",
            "SORT"                  => 10,
            "PROPERTY_TYPE"         => "N",
            "DEFAULT_VALUE"         => 0,
            "MULTIPLE"              => "N",
            "USER_TYPE"             => "SASDCheckboxNum",
            "USER_TYPE_SETTINGS"    => [
                "VIEW" => [
                    0 => "Нет",
                    1 => "Да"
                ]
            ],
            "IS_REQUIRED"           => "N"
        ],
        [
            "CODE"              => "ACTION_TYPE",
            "NAME"              => "Тип запроса",
            "ACTIVE"            => "Y",
            "SORT"              => 20,
            "PROPERTY_TYPE"     => "E",
            "MULTIPLE"          => "Y",
            "LIST_TYPE"         => "L",
            "MULTIPLE_CNT"      => 5,
            "LINK_IBLOCK_ID"    => 0,
            "LINK_IBLOCK_CODE"  => Jet5\IBlock\ApiParametersTypes::IBLOCK_CODE,
            "IS_REQUIRED"       => "Y"
        ],
        [
            "CODE"          => "DEFAULT_VALUE",
            "NAME"          => "Значение по умолчанию",
            "ACTIVE"        => "Y",
            "SORT"          => 30,
            "PROPERTY_TYPE" => "S",
            "MULTIPLE"      => "N",
            "ROW_COUNT"     => 1,
            "COL_COUNT"     => 50,
            "IS_REQUIRED"   => "N"
        ],
    ];
}