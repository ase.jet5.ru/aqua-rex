<?php
namespace Jet5\IBlock;

class ApiMethods extends ApiType
{
    const IBLOCK_CODE = 'API_METHODS';

    const IBLOCK_STRUCTURE = [
        "NAME"                  => "Методы",
        "CODE"                  => self::IBLOCK_CODE,
        "VERSION"               => 1,
        "LID"                   => self::SITE_LID,
        "ACTIVE"                => "Y",
        "SORT"                  => 10,
        "LIST_PAGE_URL"         => "",
        "SECTION_PAGE_URL"      => "",
        "DETAIL_PAGE_URL"       => "",
        "CANONICAL_PAGE_URL"    => "",
        "EDIT_FILE_BEFORE"      => ""
    ];

    const IBLOCK_FIELDS = [
        "CODE" => [
            "NAME" => "Символьный код",
            "IS_REQUIRED" => "Y",
            "DEFAULT_VALUE" => [
                "UNIQUE" => "Y",
                "TRANSLITERATION" => "Y",
                "TRANS_LEN" => 255,
                "TRANS_CASE" => "L",
                "TRANS_SPACE" => "-",
                "TRANS_OTHER" => "-",
                "TRANS_EAT" => "Y",
                "USE_GOOGLE" => "N"
            ]
        ],
    ];

    const IBLOCK_PROPERTIES = [
        [
            "CODE"          => "ACTION_TYPE",
            "NAME"          => "Тип запроса",
            "ACTIVE"        => "Y",
            "SORT"          => 10,
            "PROPERTY_TYPE" => "L",
            "MULTIPLE"      => "N",
            "LIST_TYPE"     => "L",
            "VALUES"        => [
                [
                    "VALUE"         => "GET",
                    "DEF"           => "Y",
                    "SORT"          => 10,
                    "XML_ID"        => "api_method_action_get",
                    "EXTRNAL_ID"    => "api_method_action_get"
                ],
                [
                    "VALUE"         => "POST",
                    "DEF"           => "N",
                    "SORT"          => 20,
                    "XML_ID"        => "api_method_action_post",
                    "EXTRNAL_ID"    => "api_method_action_post"
                ],
            ],
            "IS_REQUIRED"   => "Y"
        ],
        [
            "CODE"              => "PARAMETERS",
            "NAME"              => "Параметры запроса",
            "ACTIVE"            => "Y",
            "SORT"              => 20,
            "PROPERTY_TYPE"     => "E",
            "MULTIPLE"          => "Y",
            "LIST_TYPE"         => "L",
            "MULTIPLE_CNT"      => 5,
            "LINK_IBLOCK_ID"    => 0,
            "LINK_IBLOCK_CODE"  => ApiParameters::IBLOCK_CODE,
            "IS_REQUIRED"       => "N"
        ],
        [
            "CODE"          => "CACHE_TIME",
            "NAME"          => "Время хранение данных в секундах (0 - не кэшировать)",
            "ACTIVE"        => "Y",
            "SORT"          => 30,
            "PROPERTY_TYPE" => "N",
            "MULTIPLE"      => "N",
            "ROW_COUNT"     => 1,
            "COL_COUNT"     => 15,
            "IS_REQUIRED"   => "N",
            "DEFAULT_VALUE" => "0"
        ],
        [
            "CODE"          => "CLASS_NAME",
            "NAME"          => "Название класса",
            "ACTIVE"        => "Y",
            "SORT"          => 40,
            "PROPERTY_TYPE" => "S",
            "MULTIPLE"      => "N",
            "ROW_COUNT"     => 1,
            "COL_COUNT"     => 50,
            "IS_REQUIRED"   => "Y"
        ],
        [
            "CODE"          => "CLASS_FUNCTION",
            "NAME"          => "Функция класса",
            "ACTIVE"        => "Y",
            "SORT"          => 50,
            "PROPERTY_TYPE" => "S",
            "MULTIPLE"      => "N",
            "ROW_COUNT"     => 1,
            "COL_COUNT"     => 50,
            "IS_REQUIRED"   => "Y"
        ],
    ];
}