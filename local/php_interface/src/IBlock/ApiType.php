<?php
namespace Jet5\IBlock;

abstract class ApiType
{
    const SITE_LID = ['s1'];
    const IBLOCK_TYPE = 'API';
    const IBLOCK_TYPE_STRUCTURE = [
        'ID'        => self::IBLOCK_TYPE,
        'SECTIONS'  => 'N',
        'IN_RSS'    => 'N',
        'SORT'      => 500,
        'LANG'      => [
            'ru' => [
                'NAME'          => 'API',
                'ELEMENT_NAME'  => 'Элементы',
            ],
            'en' => [
                'NAME'          => 'API',
                'ELEMENT_NAME'  => 'Elements',
            ],
        ]
    ];

    public function __construct() {
    }
}