<?php
namespace Jet5\HTTP;

trait Response
{
    /**
     * Ответ на запросы к серверу
     *
     * @param array $response
     * @param int $status
     * @param array $additionalHeaders
     * @return array
     */
    public function jsonHttpResponse(array $response = [], int $status = 200, array $additionalHeaders = []): array
    {
        foreach ( $additionalHeaders as $key => $item ) {
            header("{$key}: {$item}");
        }

        header("Content-Type: application/json");
        \CHTTP::setStatus($status);

        return $response;
    }
}