<?php
namespace Jet5;

use Bitrix\Main\Application as BxApp;
use Bitrix\Main\Data\Cache as BxCache;

class Cache
{
    const API_INIT_DIR = 'api';

    private $ttl = 0;
    private $cache;

    /**
     * Cache constructor
     *
     * @throws \Bitrix\Main\SystemException
     */
    public function __construct(int $ttl = 0) {
        $this->cache = BxApp::getInstance()->getManagedCache();
        $this->setTtl(max(0, $ttl));
    }

    /**
     * Установить на какое время кешировать данные
     *
     * @param int $ttl время кеширование (в секундах)
     * @return void
     */
    public function setTtl(int $ttl = 0): void {
        $this->ttl = $ttl;
    }

    /**
     * Удаление
     *
     * @param string $code код (идентификатор)
     *
     * @return void
     */
    public function remove(string $code): void {
        $this->cache->clean($code);
    }

    /**
     * Получить данные
     *
     * @param string $code код (идентификатор)
     * @param bool $isUpdate флаг обновления
     * @param object|string $class класс или название класса
     * @param string $method название функции класса для получения данных
     * @param array $args аргументы функции класса
     *
     * @return null|array
     **/
    public function getData(string $code, bool $isUpdate = FALSE, $class = null, string $method = '', array $args = []): ?array
    {
        $data = null;
        if ( $this->ttl > 0 && !$isUpdate && $this->cache->read($this->ttl, $code) ) {
            $data = $this->cache->get($code);
        } else {
            if ( !is_null($class) && trim($method) && method_exists($class, $method) ) {
                if ( is_string($class) ) {
                    $class = new $class();
                }
                if ( count($args) > 0 ) {
                    $data = $class->$method($args);
                } else {
                    $data = $class->$method();
                }
                $this->cache->set($code, $data);
            }
        }
        return $data;
    }

    /**
     * Получить данные (static)
     *
     * @param string $code код (идентификатор)
     * @param int $ttl время жизни кэша
     *
     * @return null|array
     **/
    public static function get(string $code, int $ttl = 0, string $initDir = self::API_INIT_DIR): ?array {
        $ttl = max(0, $ttl);
        if ( $ttl > 0 ) {
            $cache = BxCache::createInstance();
            if ( $cache->initCache($ttl, $code, $initDir) ) {
                return $cache->getVars();
            }
        }
        return null;
    }

    public static function set(string $code, int $ttl, array $data, string $initDir = self::API_INIT_DIR): void
    {
        $cache = BxCache::createInstance();
        if ( $cache->initCache($ttl, $code) ) {
            $cache->clean($code);
        }
        if ( $cache->startDataCache($ttl, $code, (strlen($initDir) > 0 ? $initDir : FALSE)) ) {
            $cache->endDataCache($data);
        }
    }

    public static function clean(string $code): void {
        BxCache::createInstance()->clean($code);
    }
}
