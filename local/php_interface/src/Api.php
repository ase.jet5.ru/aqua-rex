<?php
namespace Jet5;

use Bitrix\Main\Context as BxContext;

use Jet5\HTTP\Response;
use Jet5\Model\ApiMethods as ModelApiMethods;
//use Jet5\Model\Menu as ModelMenu;

class Api
{
    use Response;

    const METHOD_NAME = 'METHOD_NAME';

    const ERR_STATUS = [
        'WRONG_REQUEST' => [
            'status'    => 400,
            'msg'       => 'Bad Request'
        ],
        'WRONG_METHOD' => [
            'status'    => 400,
            'msg'       => 'Bad Method'
        ],
    ];

    /** Метод */
    protected $method = null;
    /** Статус */
    protected int $status = 200;
    /** Флаг ошибки */
    protected bool $err = TRUE;
    /** Текст ошибки */
    protected string $errMessage = '';
    /** Флаг DEBUG */
    protected bool $isDebug = FALSE;

    /**
     * Конструктор
     */
    function __construct()
    {
        if ( $methodName = $this->_getMethodName() ) {
            $request = BxContext::getCurrent()->getRequest()->toArray();
            if ( $request['debug'] ) {
                $this->_setDebug($request['debug'] === 'Y' || intval($request['debug']) === 1);
                unset($request['debug']);
            }
            $action = BxContext::getCurrent()->getServer()->getRequestMethod();
            $apiMethods = new ModelApiMethods();
            if ( $arMethod = $apiMethods->getItem(['CODE' => $methodName, 'ACTION' => $action, 'request' => $request]) ) {
                $this->method = $arMethod;
                $this->setErr(FALSE);
            }
            else {
                $this->_setErrStatus('WRONG_METHOD');
            }
        }
        else {
            $this->_setErrStatus('WRONG_REQUEST');
        }
    }

    /**
     * Установить флаг DEBUG
     *
     * @param bool $is
     * @return void
     */
    protected function _setDebug(bool $is = FALSE): void {
        $this->isDebug = $is;
    }

    /**
     * Получить значение флага DEBUG
     *
     * @return bool
     */
    public function isDebug(): bool {
        return $this->isDebug;
    }

    /**
     * Установить статус ответа ошибки
     **
     * @param string $key
     * @return void
     **/
    protected function _setErrStatus(string $key): void
    {
        $this->setErr();
        if ( self::ERR_STATUS[ $key ] ) {
            $this->setStatus(self::ERR_STATUS[ $key ]['status']);
            $this->setErrMessage(self::ERR_STATUS[ $key ]['msg']);
        }
    }

    /**
     * Установить статус ответа
     **
     * @param int $status
     * @return void
     **/
    public function setStatus(int $status = 200)
    {
        $this->status = $status;
    }

    /**
     * Получить статус ответа
     **
     * @return int
     **/
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Ошибка ответа?
     **
     * @return bool
     **/
    public function isErr(): bool
    {
        return $this->err;
    }

    /**
     * Установить ошибку ответа
     **
     * @param bool $err
     * @return void
     **/
    public function setErr(bool $err = TRUE): void
    {
        $this->err = $err;
    }

    /**
     * Установить сообщение ошибки
     **
     * @param string $errMessage
     * @return void
     **/
    public function setErrMessage(string $errMessage = ''): void
    {
        $this->errMessage = $errMessage;
    }

    /**
     * Получить сообщение ошибки
     **
     * @return string
     **/
    public function getErrMessage(): string
    {
        return $this->errMessage;
    }

    /**
     * Выполнение запроса
     **
     * @return array
     **/
    public function exec(): array
    {
        $response = (!$this->isDebug() ? []
            : [
                'debug' => [
                    'request' => $_REQUEST,
                    'method' => $this->method,
                ],
            ]);
        if ( $this->isErr() ) {
            $response['error'] = [
                'status' => $this->getStatus(),
                'message' => $this->getErrMessage()
            ];
        }
        else {
            $class = '\\Jet5\\Model\\' . $this->method['exec']['class'];
            $classMethod = $this->method['exec']['function'];
            $data = 'Empty';
            if ( method_exists($class, $classMethod) ) {
                $args = ['cacheTime' => ($this->method['cacheTime'] ? max(0, $this->method['cacheTime']) : 0)];
                $data = (new $class())->$classMethod(array_merge($args, ($this->method['request'] ?? [])));
            }
            else {
                $data = 'Ошибка получения данных ' . $class . '->' . $classMethod . '()';
            }
            $response['data'] = $data;
        }
        return $this->jsonHttpResponse($response, $this->getStatus());
    }

    /**
     * Получить название метода
     **
     * @return null | string
     **/
    protected function _getMethodName(): ?string
    {
        return ($_REQUEST[ self::METHOD_NAME ] ?? null);
    }
}