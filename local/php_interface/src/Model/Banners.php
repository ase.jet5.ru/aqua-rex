<?php
namespace Jet5\Model;

use http\Params;
use Jet5\Application as Jet5App;
use Jet5\Cache as Jet5Cache;
use Jet5\IBlock\Banners as IBlockBanners;

class Banners
{
    const CACHE_ID = 'Banners::get';
    const ENUM_TYPE = 'banner_type_';
    const ENUM_LINK_TARGET = 'banner_link_target';

    const AR_SORT_DEF = ['SORT' => 'ASC'];
    const AR_SELECTED_FIELDS = [
        'ID', 'NAME', 'CODE', 'PROPERTY_TYPE',
        'PROPERTY_TOP_SUBTITLE_IMAGE', 'PROPERTY_TOP_SUBTITLE_TEXT',
        'PROPERTY_TITLE', 'PROPERTY_DESCRIPTION', 'PROPERTY_BG_IMAGE', 'PROPERTY_BG_COLOR',
        'PROPERTY_PACKAGE_IMAGE', 'PROPERTY_FOOD_IMAGE', 'PROPERTY_BANNER_IMAGE', 'PROPERTY_BTN_TEXT',
        'PROPERTY_LINK_HREF', 'PROPERTY_LINK_TARGET', 'PROPERTY_LINK_TITLE'
    ];

    /**
     * Constructor
     */
    public function __construct() {
    }

    protected function _getCacheId(string $lang, mixed $params = ''): string
    {
        $cacheId = $lang . '_' . self::CACHE_ID;
        if ( is_array($params) && count($params) > 0 ) { // Массив id
            foreach ( $params as $id ) {
                    $cacheId .= $id;
            }
        }
        elseif ( is_string($params) && trim($params) ) { // Код
            $cacheId .= '_' . $params;
        }
        elseif ( is_int($params) ) { // ID
            $cacheId .= '_' . intval($params);
        }
        else { // Полный список активных баннеров
            $cacheId .= '_list';
        }
        return $cacheId;
    }

    protected function _getParams(mixed $args = []): mixed
    {
        $result = null;
        if ( $args['ids'] && is_array($args['ids']) && count($args['ids']) > 0 ) { // Массив id
            foreach ( $args['ids'] as $id ) {
                if ( is_null($result) ) {
                    $result = [];
                }
                $id = intval($id);
                if ( $id > 0 ) {
                    $result[] = $id;
                }
            }
        }
        elseif ( is_string($args['code']) && trim($args['code']) ) { // Код
            $result = trim($args['code']);
        }
        elseif ( is_int($args['id']) ) { // ID
            $result = intval($args['id']);
        }
        else { // Полный список активных баннеров
            $result = [];
        }
        return $result;
    }

    public function get(array $args = []): ?array
    {
        $result = [];
        $lang = ($args['lang'] ?? null);
        if ( is_null($lang) ) {
            return null;
        }
        $cacheTime = ($args['cacheTime'] ?? 0);
        unset($args['lang'], $args['cacheTime']);

        $params = $this->_getParams($args);
        $cacheId = $this->_getCacheId($lang, $params);
        if ( $cacheTime > 0  && $result = Jet5Cache::get($cacheId, $cacheTime) ) {
        }
        else if ( $items = $this->_getActiveItemsByParams($lang, $params) ) {
            $result['items'] = $items;
            if ( $cacheTime > 0 ) {
                Jet5Cache::set(self::CACHE_ID, $cacheTime, $result);
            }
        }
        return count($result) > 0 ? $result : null;
    }

    protected function _getActiveItemsByParams(string $sectionCode, mixed $params = ''): ?array {
        $filter = [
            'SECTION_CODE' => $sectionCode
        ];
        if ( (is_array($params) && count($params) > 0) || is_int($params) ) {
            $filter['ID'] = $params;
        }
        elseif ( is_string($params) ) {
            $filter['CODE'] = $params;
        }
        return $this->getActiveItemsByFilter($filter);
    }

    public function getActiveItemsByFilter(array $filter = []): ? array
    {
        $result = [];
        $iblockId = Jet5App::getIBlockIdByCode(IBlockBanners::IBLOCK_CODE);
        $serverUrl = Jet5App::getApiServerUrl();
        $rs = \CIBlockElement::getList(
            self::AR_SORT_DEF,
            array_merge(['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y'], $filter),
            FALSE,
            FALSE,
            self::AR_SELECTED_FIELDS
        );
        while ( $ob = $rs->GetNextElement() ) {
            $fields = $ob->GetFields();
            $item = [
                'id' => $fields['ID'],
                'name' => $fields['NAME'],
                'code' => $fields['CODE'],
            ];
            $type = Jet5App::getTypeEnumByParams($iblockId, $fields['PROPERTY_TYPE_ENUM_ID'], self::ENUM_TYPE);
            switch ( $type ) {
                case 'big': {
                    $item['type'] = $type;
                    $item = array_merge($item, $this->_getBigItemFields($fields, $iblockId, $serverUrl));
                    break;
                }
                case 'standard': {
                    $item['type'] = $type;
                    $item = array_merge($item, $this->_getStandardItemFields($fields, $iblockId, $serverUrl));
                    break;
                }
                default: break;
            }
            if ( $item['type'] ) {
                $result[] = $item;
            }
        }
        return count($result) > 0 ? $result : null;
    }

    protected function _getBigItemFields(array $fields, int $iblockId, string $serverUrl = ''): array
    {
        $item = [];
        if ( $srcImage = Jet5App::getItemSrcImage('PROPERTY_BG_IMAGE_VALUE', $fields, $serverUrl) ) {
            $item['bgImage'] = $srcImage;
        }

        if ( $topSubtitle = $this->_getItemTopSubtitle($fields, $serverUrl) ) {
            $item['topSubtitle'] = $topSubtitle;
        }

        $item = array_merge($item, Jet5App::getItemHtmlValues(['PROPERTY_TITLE_VALUE' => ['field' => 'title', 'def_value' => null]], $fields));
        $item = array_merge($item, Jet5App::getItemHtmlValues(['PROPERTY_DESCRIPTION_VALUE' => ['field' => 'description', 'def_value' => null]], $fields));

        if ( $btn = $this->_getItemBtnFields($fields, $iblockId, $serverUrl) ) {
            $item = array_merge($item, ['btn' => $btn]);
        }

        if ( $srcImage = Jet5App::getItemSrcImage('PROPERTY_PACKAGE_IMAGE_VALUE', $fields, $serverUrl) ) {
            $item['packageImage'] = $srcImage;
        }
        if ( $srcImage = Jet5App::getItemSrcImage('PROPERTY_FOOD_IMAGE_VALUE', $fields, $serverUrl) ) {
            $item['foodImage'] = $srcImage;
        }
        return $item;
    }

    protected function _getStandardItemFields(array $fields, int $iblockId, string $serverUrl = ''): array
    {
        $item = [
            'title' => Jet5App::getItemValueByCode($fields, 'PROPERTY_TITLE_VALUE'),
            'description' => Jet5App::getItemValueByCode($fields, 'PROPERTY_DESCRIPTION_VALUE'),
        ];
        if ( $bgColor = Jet5App::getItemValueByCode($fields, 'PROPERTY_BG_COLOR_VALUE') ) {
            $item['bgColor'] = '#' . strtoupper($bgColor);
        }
        if ( $srcImage = Jet5App::getItemSrcImage('PROPERTY_BANNER_IMAGE_VALUE', $fields, $serverUrl) ) {
            $item['image'] = $srcImage;
        }
        if ( $link = Jet5App::getArLinkItem($fields, $iblockId, $serverUrl, self::ENUM_LINK_TARGET) ) {
            $item = array_merge($item, ['link' => $link]);
        }
        return $item;
    }

    protected function _getItemBtnFields(array $fields, int $iblockId, string $serverUrl = ''): ?array
    {
        $btn = Jet5App::getItemHtmlValues(['PROPERTY_BTN_TEXT_VALUE' => ['field' => 'text', 'def_value' => null]], $fields);
        if ( $btn['text'] && $link = Jet5App::getArLinkItem($fields, $iblockId, $serverUrl, self::ENUM_LINK_TARGET) ) {
            return array_merge($btn, ['link' => $link]);
        }
        return null;
    }

    protected function _getItemTopSubtitle(array $fields, string $serverUrl = ''): ?array
    {
        $topSubtitle = [];
        if ( $srcImage = Jet5App::getItemSrcImage('PROPERTY_TOP_SUBTITLE_IMAGE_VALUE', $fields, $serverUrl) ) {
            $topSubtitle['image'] = $serverUrl . $srcImage;
        }
        if ( $fields['~PROPERTY_TOP_SUBTITLE_TEXT_VALUE']['TEXT'] && trim($fields['~PROPERTY_TOP_SUBTITLE_TEXT_VALUE']['TEXT']) ) {
            $topSubtitle['text'] = trim($fields['~PROPERTY_TOP_SUBTITLE_TEXT_VALUE']['TEXT']);
        }
        elseif ( $fields['PROPERTY_TOP_SUBTITLE_TEXT_VALUE']['TEXT'] && trim($fields['PROPERTY_TOP_SUBTITLE_TEXT_VALUE']['TEXT']) ) {
            $topSubtitle['text'] = trim($fields['~PROPERTY_TOP_SUBTITLE_TEXT_VALUE']['TEXT']);
        }
        return count($topSubtitle) > 0 ? $topSubtitle : null;
    }
}