<?php
namespace Jet5\Model;

use Jet5\Application as Jet5App;
use Jet5\Cache as Jet5Cache;
use Jet5\IBlock\ApiMethods as IBlockApiMethods;
use Jet5\IBlock\ApiParameters as IBlockApiParameters;

class ApiMethods
{
    /**
     * Конструктор
     */
    function __construct() {
    }

    protected function _getItemByParameters(array $args = []): ?array
    {
        $method = [];
        $filter = [
            'IBLOCK_ID' => Jet5App::getIBlockIdByCode(IBlockApiMethods::IBLOCK_CODE),
            'ACTIVE' => 'Y',
            'CODE' => ($args['CODE'] ? strtolower(trim($args['CODE'])) : null),
            'PROPERTY_ACTION_TYPE_VALUE' => ($args['ACTION'] ? strtoupper(trim($args['ACTION'])) : null),
        ];
        if ( $filter['CODE'] && trim($filter['CODE']) && $filter['PROPERTY_ACTION_TYPE_VALUE'] && trim($filter['PROPERTY_ACTION_TYPE_VALUE'])) {
            $selectFields = [
                'ID', 'NAME', 'CODE',
                'PROPERTY_PARAMETERS', 'PROPERTY_CACHE_TIME', 'PROPERTY_CLASS_NAME', 'PROPERTY_CLASS_FUNCTION'
            ];
            $rs = \CIBlockElement::getList([], $filter, FALSE, FALSE, $selectFields);
            if ( $ob = $rs->GetNextElement() ) {
                $fields = $ob->GetFields();
                //return $fields;
                $method = [
                    'code' => $fields['CODE'],
                    'cacheTime' => intval($fields['PROPERTY_CACHE_TIME_VALUE']),
                    'exec' => [
                        'class' => $fields['PROPERTY_CLASS_NAME_VALUE'],
                        'function' => $fields['PROPERTY_CLASS_FUNCTION_VALUE']
                    ]
                ];
                $request = ($args['request'] ?? null);
                if ( $fields['PROPERTY_PARAMETERS_VALUE'] && count($fields['PROPERTY_PARAMETERS_VALUE']) > 0 ) {
                    $method['request'] = [];
                    if ( $parameters = $this->_getParametersByArIds($fields['PROPERTY_PARAMETERS_VALUE']) ) {
                        foreach ( $parameters as $k_parameter => $v_parameter ) {
                            if ( $request[ $k_parameter ] ) {
                                if ( $itemRequest = $this->_getParameterValueByType($v_parameter['type'], $k_parameter, $request[ $k_parameter ], $v_parameter['separator']) ) {
                                    $method['request'][ $k_parameter ] = $itemRequest[ $k_parameter ];
                                }
                            }
                            if ( $v_parameter['isRequired'] && !isset($method['request'][ $k_parameter ]) ) {
                                if ( $itemRequest = $this->_getParameterValueByType($v_parameter['type'], $k_parameter, $v_parameter['defaultValue'], $v_parameter['separator']) ) {
                                    $method['request'][ $k_parameter ] = $itemRequest[ $k_parameter ];
                                }
                            }
                        }
                    }
                    if ( count($method['request']) == 0 ) {
                        unset($method['request']);
                    }
                }
            }
        }
        return count($method) > 0 ? $method : null;
    }

    protected function _getParametersByArIds(array $arIds): ?array
    {
        $parameters = [];
        $filter = [
            'IBLOCK_ID' => Jet5App::getIBlockIdByCode(IBlockApiParameters::IBLOCK_CODE),
            'ACTIVE' => 'Y',
            'ID' => $arIds
        ];
        $selectFields = [
            'ID', 'NAME', 'CODE',
            'PROPERTY_IS_REQUIRED', 'PROPERTY_DEFAULT_VALUE', 'PROPERTY_TYPE.CODE', 'PROPERTY_TYPE.PROPERTY_ARR_SEPARATOR'
        ];
        $rs = \CIBlockElement::getList(['SORT' => 'ASC'], $filter, FALSE, FALSE, $selectFields);
        while ( $ob = $rs->GetNextElement() ) {
            $fields = $ob->GetFields();
            $item = [
                'isRequired' => intval($fields['PROPERTY_IS_REQUIRED_VALUE']) === 1,
                'type' => $fields['PROPERTY_TYPE_CODE'],
                'defaultValue' => trim($fields['PROPERTY_DEFAULT_VALUE_VALUE']),
                'separator' => trim($fields['PROPERTY_TYPE_PROPERTY_ARR_SEPARATOR_VALUE'])
            ];
            $parameters[ $fields['CODE'] ] = $item;
        }
        return count($parameters) > 0 ? $parameters : null;
    }

    protected function _getParameterValueByType(string $type, string $parameterName, string $value, string $separator = ','): ?array
    {
        $parameter = [];
        switch ( $type ) {
            case 'string': {
                $parameter[ $parameterName ] = trim($value);
                break;
            }
            case 'array': {
                if ( $separator && trim($separator) ) {
                    $arVal = explode(trim($separator), $value);
                    if ( count($arVal) > 0 ) {
                        $parameter[ $parameterName ] = [];
                        foreach ( $arVal as $v_val ) {
                            if ( $v_val && trim($v_val) ) {
                                $parameter[ $parameterName ][] = trim($v_val);
                            }
                        }
                    }
                }
                break;
            }
            case 'integer': {
                $parameter[ $parameterName ] = intval(trim($value));
                break;
            }
            case 'double': {
                $parameter[ $parameterName ] = floatval(trim($value));
                break;
            }
            default: break;
        }
        return count($parameter) > 0 ? $parameter : null;
    }

    public function getItem(array $args = []): ?array
    {
        if ( isset($args['CODE']) ) {
            $cacheId = md5(strtolower(trim($args['CODE'])));
            $cacheTime = max(0, intval(Jet5App::getCustomSettings('API', 'methods_cache_time')));
            if ( $cacheTime > 0 && $item = Jet5Cache::get($cacheId, $cacheTime, Jet5Cache::API_INIT_DIR) ) {
                return $item;
            }
            else {
                Jet5Cache::clean($cacheId);
            }
            if ( $item = $this->_getItemByParameters($args) ) {
                Jet5Cache::set($cacheId, $cacheTime, $item, Jet5Cache::API_INIT_DIR);
                return $item;
            }
        }
        return null;
    }
}