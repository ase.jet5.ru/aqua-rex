<?php
namespace Jet5\Model;

use Jet5\Application as Jet5App;
use Jet5\Cache as Jet5Cache;
use Jet5\IBlock\ContentMenu as IBlockMenu;

class Menu
{
    const CACHE_ID = 'Menu::get';
    const ENUM_TYPE = 'menu_type_';
    const ENUM_LINK_TARGET = 'menu_link_target';
    const CONTENT_IMAGE_START = '#IMAGE_START#';
    const CONTENT_IMAGE_END = '#IMAGE_END#';

    /**
     * Constructor
     */
    public function __construct() {
    }

    protected function _getChacheId(string $lang): string {
        return $lang . '_' . self::CACHE_ID;
    }

    public function get(array $args = []): ?array
    {
        $result = [];
        $lang = ($args['lang'] ?? null);
        if ( is_null($lang) ) {
            return null;
        }
        $cacheTime = ($args['cacheTime'] ?? 0);
        if ( $cacheTime > 0 && $result = Jet5Cache::get($this->_getChacheId($lang), $cacheTime) ) {
        }
        else if ( $items = $this->_getActiveItemsBySectionCode($lang) ) {
            $result['items'] = $items;
            if ( $cacheTime > 0 ) {
                Jet5Cache::set(self::CACHE_ID, $cacheTime, $result);
            }
        }
        return count($result) > 0 ? $result : null;
    }

    protected function _getActiveItemsBySectionCode(string $sectionCode): ?array
    {
        $result = [];
        $arLink = [];
        $arLang = [];
        //$items = [];
        $iblockId = Jet5App::getIBlockIdByCode(IBlockMenu::IBLOCK_CODE);
        $serverUrl = Jet5App::getApiServerUrl();
        $filter = [
            'IBLOCK_ID' => $iblockId,
            'ACTIVE' => 'Y',
            'SECTION_CODE' => $sectionCode,
        ];
        $selectFields = [
            'ID', 'NAME', 'CODE',
            'PROPERTY_NAME', 'PROPERTY_NAME_MOB', 'PROPERTY_TYPE', 'PROPERTY_IMAGE',
            'PROPERTY_LINK_HREF', 'PROPERTY_LINK_TARGET', 'PROPERTY_LINK_TITLE'
        ];
        $rs = \CIBlockElement::getList(['SORT' => 'ASC'], $filter, FALSE, FALSE, $selectFields);
        while ( $ob = $rs->GetNextElement() ) {
            $fields = $ob->GetFields();
            if ( $fields['PROPERTY_TYPE_ENUM_ID'] ) {
                $type = $this->_getTypeItem($iblockId, $fields['PROPERTY_TYPE_ENUM_ID'], self::ENUM_TYPE);
                switch ( $type ) {
                    case 'logo': {
                        if (!key_exists('logo', $result)) {
                            $result['logo'] = [];
                        }
                        if ( $fields['PROPERTY_IMAGE_VALUE'] && $imgFile = \CFile::getFileArray($fields['PROPERTY_IMAGE_VALUE']) ) {
                            $result['logo']['image'] = $serverUrl . $imgFile['SRC'];
                        }
                        if ( $link = $this->_getFieldsLink($fields, $iblockId, $serverUrl) ) {
                            $result['logo']['link'] = $link;
                        }
                        break;
                    }
                    case 'btn': {
                        if ( $item = $this->_getBtnItem($fields, $iblockId, $serverUrl) ) {
                            $result['btn'] = $item;
                        }
                        break;
                    }
                    case 'link': {
                        if ( $item = $this->_getLinkItem($fields, $iblockId, $serverUrl) ) {
                            $arLink[] = $item;
                        }
                        break;
                    }
                    case 'lang': {
                        if ( $item = $this->_getLangItem($fields, $iblockId, $serverUrl, $sectionCode) ) {
                            $arLang[] = $item;
                        }
                        break;
                    }
                    case 'popup': {
                        if ( $item = $this->_getPopupItem($fields, $iblockId, $serverUrl) ) {
                            $result['popup'] = $item;
                        }
                        break;
                    }
                    default: break;
                }
                //$items[] = $fields;
            }
            if ( count($arLink) > 0 ) {
                $result['links'] = $arLink;
            }
            if ( count($arLang) > 0 ) {
                $result['lang'] = $arLang;
            }
        }
        /*if ( count($items) > 0 ) {
            $result['items'] = $items;
        }*/
        return count($result) > 0 ? $result : null;
    }

    protected function _getTypeItem(int $iblockId, string $enumId, string $selType): string
    {
        $type = '';
        $propertyEnums = \CIBlockPropertyEnum::GetList([], ['IBLOCK_ID' => $iblockId, 'ID' => $enumId]);
        if ( $enumFields = $propertyEnums->GetNext() ) {
            if ( $enumFields['XML_ID'] ) {
                $type = str_replace($selType, '', $enumFields['XML_ID']);
            }
        }
        return $type;
    }

    protected function _getBtnItem(array $fields, int $iblockId = 0, string $serverUrl = ''): ?array
    {
        $item = [];
        if ( $fields['PROPERTY_IMAGE_VALUE'] && $imgFile = \CFile::getFileArray($fields['PROPERTY_IMAGE_VALUE']) ) {
            $item['image'] = $serverUrl . $imgFile['SRC'];
        }
        $textHtml = ($fields['~PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['~PROPERTY_NAME_VALUE']['TEXT'])
            : ($fields['PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['PROPERTY_NAME_VALUE']['TEXT']) : $fields['NAME']));
        if ( ($posStart = strpos($textHtml, self::CONTENT_IMAGE_START)) !== FALSE
            && ($posEnd = strpos($textHtml, self::CONTENT_IMAGE_END)) !== FALSE ) {
            $before = substr($textHtml, 0, $posStart);
            $textHtml = substr($textHtml, strlen($before) + strlen(self::CONTENT_IMAGE_START));
            if ( ($posEnd = strpos($textHtml, self::CONTENT_IMAGE_END)) !== FALSE ) {
                $in = substr($textHtml, 0, $posEnd);
                $textHtml =    substr($textHtml, strlen($in) + strlen(self::CONTENT_IMAGE_END));
                $item['content'] = [
                    'before' => $before,
                    'in' => $in,
                    'after' => $textHtml,
                ];
            }
            else {
                $item['content'] = [
                    'before' => $before,
                    'in' => $textHtml,
                ];
            }
        }
        else {
            $item['text'] = $textHtml;
        }
        if ( $link = $this->_getFieldsLink($fields, $iblockId, $serverUrl) ) {
            $item['link'] = $link;
        }
        return count($item) > 0 ? $item : null;
    }

    protected function _getLinkItem(array $fields, int $iblockId = 0, string $serverUrl = ''): ?array
    {
        $item = [];
        if ( $link = $this->_getFieldsLink($fields, $iblockId, $serverUrl) ) {
            $item = [
                'code' => $fields['CODE'],
                'name' => ($fields['~PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['~PROPERTY_NAME_VALUE']['TEXT'])
                    : ($fields['PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['PROPERTY_NAME_VALUE']['TEXT']) : $fields['NAME'])),
                'link' => $link
            ];
        }
        return count($item) > 0 ? $item : null;
    }

    protected function _getLangItem(array $fields, int $iblockId = 0, string $serverUrl = '', string $lang = 'ru'): ?array
    {
        $item = [];
        if ( $link = $this->_getFieldsLink($fields, $iblockId, $serverUrl) ) {
            $item = [
                'code' => $fields['CODE'],
                'name' => ($fields['~PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['~PROPERTY_NAME_VALUE']['TEXT'])
                    : ($fields['PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['PROPERTY_NAME_VALUE']['TEXT']) : $fields['NAME'])),
            ];
            if ( $fields['CODE'] == $lang ) {
                $item['selected'] = TRUE;
            }
            else {
                $item['link'] = $link;
            }
        }
        return count($item) > 0 ? $item : null;
    }

    protected function _getPopupItem(array $fields, int $iblockId = 0, string $serverUrl = ''): array
    {
        $item = [
            'code' => $fields['CODE'],
            'name' => ($fields['~PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['~PROPERTY_NAME_VALUE']['TEXT'])
                : ($fields['PROPERTY_NAME_VALUE']['TEXT'] ? trim($fields['PROPERTY_NAME_VALUE']['TEXT']) : $fields['NAME'])),
        ];
        if ( $fields['PROPERTY_IMAGE_VALUE'] && $imgFile = \CFile::getFileArray($fields['PROPERTY_IMAGE_VALUE']) ) {
            $item['image'] = $serverUrl . $imgFile['SRC'];
        }
        return $item;
    }

    protected function _getFieldsLink(array $fields, int $iblockId = 0, string $serverUrl = ''): ?array
    {
        $href = ($fields['~PROPERTY_LINK_HREF_VALUE'] ? trim($fields['~PROPERTY_LINK_HREF_VALUE'])
            : ($fields['PROPERTY_LINK_HREF_VALUE'] ? trim($fields['PROPERTY_LINK_HREF_VALUE']) : null));
        if ( is_null($href) ) {
            return null;
        }
        $link = [
            'href' => str_replace('#SITE#', $serverUrl, $href),
            'target' => '_self',
            'title' => ($fields['~PROPERTY_LINK_TITLE_VALUE'] ? trim($fields['~PROPERTY_LINK_TITLE_VALUE'])
                : ($fields['PROPERTY_LINK_TITLE_VALUE'] ? trim($fields['PROPERTY_LINK_TITLE_VALUE']) : $fields['NAME']))
        ];
        if ( $fields['PROPERTY_LINK_TARGET_ENUM_ID'] ) {
            if ($iblockId == 0) {
                $iblockId = Jet5App::getIBlockIdByCode(IBlockMenu::IBLOCK_CODE);
            }
            if ( $target = $this->_getTypeItem($iblockId, $fields['PROPERTY_LINK_TARGET_ENUM_ID'], self::ENUM_LINK_TARGET) ) {
                $link['target'] = $target;
            }
        }
        return $link;
    }
}