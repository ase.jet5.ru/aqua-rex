<?php
namespace Jet5;

use CIBlockElement;
use Bitrix\Main\Config\Option as BxOption;
use Bitrix\Main\Context as BxContext;

class Application
{
    /**
     * Массив дополнительных настроек
     */
    const AR_SETTINGS = [
        'API' => [
            'server_url' => 'CS_SERVER_URL',
            'methods_cache_time' => 'CS_METHODS_CACHETIME'
        ],
    ];

    const AR_SEO = [
        'ELEMENT_META_TITLE' => 'title',
        'ELEMENT_META_KEYWORDS' => 'keywords',
        'ELEMENT_META_DESCRIPTION' => 'description',
    ];

    const SEO_CONTENT_ID = 'seo_page';

    const APP_ENUM_LINK_TARGET = 'menu_link_target';
    const APP_LINK_FIELDS = [
        'href' => 'PROPERTY_LINK_HREF_VALUE',
        'target' => 'PROPERTY_LINK_TARGET_ENUM_ID',
        'title' => 'PROPERTY_LINK_TITLE_VALUE'
    ];

    /**
     * Получить ID инфоблока по коду
     **
     * @param string $code
     * @return string
     **/
    public static function getIBlockIdByCode(string $code): ?string {
        \Bitrix\Main\Loader::includeModule("iblock");
        return \CIBlock::GetList([], ['CODE' => $code], false, false, ['ID'])->fetch()['ID'];
    }

    /**
     * Получить "часть" тип (XML_ID) из поля список
     **/
    public static function getTypeEnumByParams(int $iblockId, string $enumId, string $selType = ''): string
    {
        $type = '';
        $propertyEnums = \CIBlockPropertyEnum::GetList([], ['IBLOCK_ID' => $iblockId, 'ID' => $enumId]);
        if ( $enumFields = $propertyEnums->GetNext() ) {
            if ( $enumFields['XML_ID'] ) {
                if ( trim($selType) ) {
                    $type = str_replace($selType, '', $enumFields['XML_ID']);
                }
                else {
                    $type = $enumFields['XML_ID'];
                }
            }
        }
        return $type;
    }

    /**
     * Получить ID дополнительных настроек
     **
     * @param string $tab
     * @param string $key
     * @return string|null
     **/
    public static function getIdSettingsByKey(string $tab, string $key): ?string {
        return (self::AR_SETTINGS[ $tab ][ $key ] ?? null);
    }

    /**
     * Получить ЗНАЧЕНИЕ дополнительной настройки
     **
     * @param string $tab
     * @param string $key
     * @return string|null
     **/
    public static function getCustomSettings(string $tab, string $key): ?string {
        $settings_id = self::getIdSettingsByKey($tab, $key);
        return (is_null($settings_id) ? null : BxOption::get('grain.customsettings', $settings_id));
    }

    public static function getApiServerUrl(): ?string {
        if ( $serverUrl = self::getCustomSettings('API', 'server_url') ) {
            $serverUrl = BxContext::getCurrent()->getServer()->get('REQUEST_SCHEME') . '://' . $serverUrl;
        }
        return $serverUrl;
    }

    public static function getApiMethodCacheId(string $method, string $class, string $function, array $request = []): string
    {
        $cacheId = $method . '|' . $class . '::' . $function;
        if ( count($request) ) {
            foreach ( $request as $k => $v ) {
                $cacheId .= '.' . $k . '=' . $v;
            }
        }
        return md5($cacheId);
    }

    public static function getSeoForIBlockElement(int $iblockId, int $elemId): ?array
    {
        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($iblockId, $elemId);
        if ( $getSeo = $ipropValues->getValues() ) {
            $arSeo = [];
            foreach ( self::AR_SEO as $k => $v ) {
                $arSeo[ $v ] = ($getSeo[ $k ] ? trim($getSeo[ $k ]) : '');
            }
            return $arSeo;
        }
        return null;
    }

    public static function getSettings(): array {
        return [
            'detail_url' => self::getCustomSettings('YASENFILM', 'detail_url_work'),
            'detail_btn_text' => self::getCustomSettings('YASENFILM', 'defail_btn_text'),
            'cache_time' => max(0, intval(self::getCustomSettings('YASENFILM', 'cache_time'))),
            'cache_dir' => self::getCustomSettings('YASENFILM', 'cache_dir'),
            'other_works' => self::getCustomSettings('YASENFILM', 'other_works'),
        ];
    }

    public static function getSeoContent(array $seo = []): string
    {
        $content = '';
        if ( count($seo) > 0 ) {
            $arSeo = [
                'title' => '<title>#VALUE#</title>',
                'keywords' => '<meta name="keywords" content="#VALUE#" />',
                'description' => '<meta name="description" content="#VALUE#" />',
            ];
            foreach ( $arSeo as $k_seo => $v_seo ) {
                if ( $seo[ $k_seo ] ) {
                    $content .= str_replace('#VALUE#', $seo[ $k_seo ], $v_seo) . PHP_EOL;
                }
            }
        }
        return $content;
    }

    public static function getItemHtmlValues(array $arFields, array $fields): array
    {
        $item = [];
        foreach ($arFields as $k_field => $v_field) {
            $item[$v_field['field']] = ($fields['~' . $k_field]['TEXT'] && trim($fields['~' . $k_field]['TEXT'])
                ? trim($fields['~' . $k_field]['TEXT'])
                : ($fields[$k_field]['TEXT'] && trim($fields[$k_field]['TEXT'])
                    ? trim($fields[$k_field]['TEXT']) : $v_field['def_value']));
        }
        return $item;
    }

    public static function getItemValueByCode(array $fields, string $code, mixed $defValue = null): ?string
    {
        $value = null;
        if ( $fields[ $code ] ) {
            $prefix = ['~', ''];
            if ( is_array($fields[ $code]) ) {
                foreach ( $prefix as $v ) {
                    if ( $fields[ $v . $code ] && $fields[ $v . $code ]['TEXT'] && trim($fields[ $v . $code ]['TEXT']) ) {
                        return trim($fields[ $v . $code ]['TEXT']);
                    }
                }
            }
            elseif ( is_string($fields[ $code ]) ) {
                foreach ( $prefix as $v ) {
                    if ( $fields[ $v . $code ] && trim($fields[ $v . $code ]) ) {
                        return trim($fields[ $v . $code ]);
                    }
                }
            }
            $value = $defValue;
        }
        return $value;
    }

    public static function getItemSrcImage(string $code, array $fields, string $serverUrl = ''): ?string
    {
        $srcImage = null;
        if ( $fields[ $code ] && $imgFile = \CFile::getFileArray($fields[ $code ]) ) {
            $srcImage = $serverUrl . $imgFile['SRC'];
        }
        return $srcImage;
    }

    public static function getArLinkItem(array $fields, int $iblockId, string $serverUrl = '', string $selType = self::APP_ENUM_LINK_TARGET): ?array
    {
        $href = self::getItemValueByCode($fields, self::APP_LINK_FIELDS['href']);
        if ( is_null($href) ) {
            return null;
        }
        $link = ['href' => str_replace('#SITE#', $serverUrl, $href)];
        if ( $fields[ self::APP_LINK_FIELDS['target'] ] && $iblockId > 0 ) {
            $link['target'] = self::getTypeEnumByParams($iblockId, $fields[ self::APP_LINK_FIELDS['target'] ], $selType);
        }
        else {
            $link['target'] = '_self';
        }
        $link['title'] = self::getItemValueByCode($fields, self::APP_LINK_FIELDS['title'], '');
        return $link;
    }
}