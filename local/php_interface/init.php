<?php
/**
 * ПОДКЛЮЧЕНИЕ КОНСТАНТ
 */
include $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . 'local' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'const.inc.php';

require_once __DIR__ . '/autoload.php';

/**
* Удалить вкладку "Реклама"
**/
AddEventHandler('main', 'OnAdminTabControlBegin', 'removeYandexDirectTab');
function removeYandexDirectTab(&$TabControl): void
{
    if ( $GLOBALS['APPLICATION']->GetCurPage() == '/bitrix/admin/iblock_element_edit.php' ) {
        foreach( $TabControl->tabs as $Key => $arTab ) {
            if ( $arTab['DIV'] == 'seo_adv_seo_adv') {
                unset($TabControl->tabs[ $Key ]);
            }
        }
    }
}
