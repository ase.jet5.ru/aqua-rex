<?php
/*******
 *
 * ОПРЕДЕЛЕНИЕ КОНСТАНТ и НАСТРОЕК PHP
 * 
 *******/

// SESSION
//session_start();

// установка часового пояса - Новосибирск
date_default_timezone_set('Asia/Novosibirsk');

/**
 * Разделитель
 **/
define( 'DIR_SEP', DIRECTORY_SEPARATOR );

/**
 * Корневая папка сайта
 **/
define( 'ROOT_PATH', $_SERVER["DOCUMENT_ROOT"] . DIR_SEP );

/**
 * 404
 **/
//define( '404_PATH', ROOT_PATH . '404.php' );

/**
 * Папка LOCAL
 **/
define( 'LOCAL_PATH', ROOT_PATH . 'local' . DIR_SEP );
define( 'LOCAL_URL',  DIR_SEP .   'local' . DIR_SEP );

/**
 * Папка для хранения исполняющих файлов включения
 **/
define( 'INC_PATH', LOCAL_PATH . 'includes' . DIR_SEP );
define( 'INC_URL', LOCAL_URL . 'includes' . DIR_SEP );

/**
 * Папка для хранения файлов включения
 **/
define( 'INC_FILE_PATH', ROOT_PATH . 'includes' . DIR_SEP );
define( 'INC_FILE_URL', DIR_SEP . 'includes' . DIR_SEP );

/**
 * Папка для хранения файлов включения AJAX
 **/
define( 'INC_FILE_AJAX_PATH', INC_FILE_PATH . 'ajax' . DIR_SEP );
define( 'INC_FILE_AJAX_URL', INC_FILE_URL . 'ajax' . DIR_SEP );

/**
 * Папка для внешних библиотек
 **/
define( 'LIB_PATH', LOCAL_PATH . 'library' . DIR_SEP );

/**
 * Папка для классов
 **/
define( 'CLASS_PATH', LOCAL_PATH . 'classes' . DIR_SEP);

/**
 * Папка для модулей
 **/
define( 'MOD_PATH', LOCAL_PATH . 'modules' . DIR_SEP);

/**
 * Папка для MEDIA
 **/
define( 'MEDIA_PATH', LOCAL_PATH . 'media' . DIR_SEP );
define( 'MEDIA_URL',  LOCAL_URL .  'media' . DIR_SEP );

/**
 * Папка для MEDIA - JS
 **/
define( 'MEDIA_JS_PATH', MEDIA_PATH . 'js' . DIR_SEP );
define( 'MEDIA_JS_URL',  MEDIA_URL .  'js' . DIR_SEP );

/**
 * Папка для MEDIA - CSS
 **/
define( 'MEDIA_CSS_PATH', MEDIA_PATH . 'css' . DIR_SEP );
define( 'MEDIA_CSS_URL',  MEDIA_URL .  'css' . DIR_SEP );

/**
 * Папка для MEDIA - IMAGES
 **/
define( 'MEDIA_IMG_PATH', MEDIA_PATH . 'img' . DIR_SEP );
define( 'MEDIA_IMG_URL',  MEDIA_URL .  'img' . DIR_SEP );

/**
 * Папка для MEDIA - FONTS
 **/
define( 'MEDIA_FONTS_PATH', MEDIA_PATH . 'font' . DIR_SEP );
define( 'MEDIA_FONTS_URL',  MEDIA_URL .  'font' . DIR_SEP );

/**
 * Папка для хранения временных файлов
 **/
define( 'UPLOAD_PATH',  ROOT_PATH . 'upload' . DIR_SEP);
define( 'UPLOAD_TMP_PATH',  UPLOAD_PATH . 'tmp' . DIR_SEP);
define( 'UPLOAD_TMP_URL',  DIR_SEP .   'upload' . DIR_SEP . 'tmp' . DIR_SEP);
define( 'UPLOAD_UFILES_PATH',  UPLOAD_PATH . 'user_files' . DIR_SEP);
define( 'UPLOAD_UFILES_URL',  DIR_SEP .   'upload' . DIR_SEP . 'user_files' . DIR_SEP);

/**
 * Папка для хранения logs файлов
 **/
define( 'LOG_PATH',  UPLOAD_PATH . 'logs' . DIR_SEP );
/**
 * Папка для хранения logs файлов
 **/
define( 'IMPORT_PATH',  UPLOAD_PATH . 'import' . DIR_SEP );

/**
 * E-mail адреса для мониторинга
 **/
define( 'MONITOR_EMAIL_FROM', 'debug@kruche.ru' );	// отправителя
define( 'MONITOR_EMAIL_TO', 'ase@jet5.ru' );		// получателя

?>