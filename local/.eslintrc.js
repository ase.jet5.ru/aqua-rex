module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "airbnb-base",
        "@vue/airbnb",
        "plugin:vue/vue3-essential",
        "prettier"
    ],
    "rules": {
        "no-restricted-globals": ["error", "stub"],
        // "no-param-reassign": ["error", { props: false }],
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                js: "never",
                jsx: "never",
                ts: "never",
                tsx: "never",
            },
        ],
        "lines-between-class-members": ["off"],
        "arrow-parens": ["error", "as-needed"],
        "no-empty": ["off"],
        "semi": ["off"],
        /* Party begins here */
        "padding-line-between-statements": [
            "error",
            {
                blankLine: "always",
                prev: "*",
                next: [
                    "return",
                    "block",
                    "block-like",
                    "throw",
                    "singleline-const",
                    "singleline-let",
                    "try",
                ],
            },
        ],
        "no-param-reassign": 0,
        "no-plusplus": 0,
        "prefer-destructuring": 0,
        "no-case-declarations": 0,
        "no-extra-semi": 1,
        "no-trailing-spaces": 0,
        "no-useless-return": 0,
        "no-var": 0,
        "default-case": [
            "error",
            {
                commentPattern: "^skip\\sdefault",
            },
        ],
        "vue/no-async-in-computed-properties": "error",
        "vue/no-dupe-keys": "error",
        "vue/no-dupe-v-else-if": "error",
        "vue/no-duplicate-attributes": "error",
        "vue/no-mutating-props": "error",
        "vue/no-parsing-error": [
            "error",
            {
                "abrupt-closing-of-empty-comment": true,
                "absence-of-digits-in-numeric-character-reference": true,
                "cdata-in-html-content": true,
                "character-reference-outside-unicode-range": true,
                "control-character-in-input-stream": true,
                "control-character-reference": true,
                "eof-before-tag-name": true,
                "eof-in-cdata": true,
                "eof-in-comment": true,
                "eof-in-tag": true,
                "incorrectly-closed-comment": true,
                "incorrectly-opened-comment": true,
                "invalid-first-character-of-tag-name": true,
                "missing-attribute-value": true,
                "missing-end-tag-name": true,
                "missing-semicolon-after-character-reference": true,
                "missing-whitespace-between-attributes": true,
                "nested-comment": true,
                "noncharacter-character-reference": true,
                "noncharacter-in-input-stream": true,
                "null-character-reference": true,
                "surrogate-character-reference": true,
                "surrogate-in-input-stream": true,
                "unexpected-character-in-attribute-name": true,
                "unexpected-character-in-unquoted-attribute-value": true,
                "unexpected-equals-sign-before-attribute-name": true,
                "unexpected-null-character": true,
                "unexpected-question-mark-instead-of-tag-name": true,
                "unexpected-solidus-in-tag": true,
                "unknown-named-character-reference": true,
                "end-tag-with-attributes": true,
                "duplicate-attribute": true,
                "end-tag-with-trailing-solidus": true,
                "non-void-html-element-start-tag-with-trailing-solidus": false,
                "x-invalid-end-tag": true,
                "x-invalid-namespace": true,
            },
        ],
        "vue/no-side-effects-in-computed-properties": "error",
        "vue/no-template-key": "error",
        "vue/no-unused-components": "error",
        "vue/no-unused-vars": "error",
        "vue/require-prop-type-constructor": "error",
        "vue/require-v-for-key": "error",
        "vue/require-valid-default-prop": "error",
        "vue/return-in-computed-property": "error",
        "vue/attribute-hyphenation": ["error", "always"],
        "vue/component-definition-name-casing": ["error", "kebab-case"],
        "vue/html-closing-bracket-newline": [
            "error",
            {
                singleline: "never",
                multiline: "always",
            },
        ],
        "vue/html-closing-bracket-spacing": [
            "error",
            {
                startTag: "never",
                endTag: "never",
                selfClosingTag: "always",
            },
        ],
        "vue/html-end-tags": "error",
        "vue/html-quotes": [
            "error",
            "double",
            {
                avoidEscape: false,
            },
        ],
        "vue/html-self-closing": [
            "error",
            {
                html: {
                    normal: "never",
                    void: "always",
                    component: "always",
                },
                svg: "always",
                math: "always",
            },
        ],
        "vue/max-attributes-per-line": [
            "error",
            {
                singleline: 20,
                multiline: 1,
            },
        ],
        "vue/multiline-html-element-content-newline": [
            "error",
            {
                ignoreWhenEmpty: true,
                allowEmptyLines: true,
            },
        ],
        "vue/mustache-interpolation-spacing": ["error", "always"],
        "vue/no-spaces-around-equal-signs-in-attribute": "error",
        "vue/no-template-shadow": "error",
        "vue/prop-name-casing": ["error", "camelCase"],
        "vue/require-default-prop": "error",
        "vue/require-prop-types": "error",
        "vue/singleline-html-element-content-newline": 0,
        "vue/v-slot-style": [
            "error",
            {
                atComponent: "shorthand",
                default: "shorthand",
                named: "shorthand",
            },
        ],
        "vue/attributes-order": [
            "error",
            {
                order: [
                    "DEFINITION",
                    "LIST_RENDERING",
                    "CONDITIONALS",
                    "RENDER_MODIFIERS",
                    "GLOBAL",
                    "UNIQUE",
                    "TWO_WAY_BINDING",
                    "OTHER_DIRECTIVES",
                    "OTHER_ATTR",
                    "CONTENT",
                    "EVENTS",
                ],
                alphabetical: false,
            },
        ],
        "vue/order-in-components": [
            "error",
            {
                order: [
                    "el",
                    "name",
                    "key",
                    "parent",
                    "functional",
                    "mixins",
                    ["components", "directives", "filters"],
                    ["props", "propsData"],
                    ["provide", "inject"],
                    "model",
                    "layout",
                    "middleware",
                    "validate",
                    "scrollToTop",
                    "transition",
                    "loading",
                    "inheritAttrs",
                    "data",
                    "fetch",
                    "head",
                    "computed",
                    "watch",
                    "ROUTER_GUARDS",
                    "LIFECYCLE_HOOKS",
                    "methods",
                ],
            },
        ],
        "vue/component-tags-order": [
            "error",
            {
                order: [["script", "template"], "style"],
            },
        ],
        "vue/this-in-template": ["error", "never"],
    },
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script"
            }
        }
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "vue"
    ]
}
