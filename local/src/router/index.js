import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'


const scrollBehavior = function (to, from, savedPosition) {
  if (to.hash) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ el: to.hash, top: 50, behavior: 'smooth',})
      }, 0)
    })
  }
  else if (savedPosition) {
    return savedPosition // скролл по кнопке назад
  }
  else{
    return { top: 0}; // скролл к началу страницы
  }

}


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: `/:lang`,
      name: 'home',
      component: Home
    },
    {
      path: `/:lang/about`,
      name: 'about',
      component: () => import('../views/About.vue')
    }    ,
    {
      path: `/:lang/:fishfeeds`,
      name: 'assortment',
      component: () => import('../views/Assortment.vue'),
    },
    {
      path: `/:lang/:fishfeeds/:feeditem`,
      name: 'detailFeed',
      component: () => import('../views/DetailFeed.vue')
    },
    {
      path: `/:lang/contacts`,
      name: 'contacts',
      component: () => import('../views/PageContacts.vue')
    },
    {
      path: `/:lang/news`,
      name: 'news',
      component: () => import('../views/PressCenter.vue'),
    },
    {
      path: `/:lang/news/:news_id`,
      name: 'detailNews',
      component: () => import('../views/DetailNews.vue')
    }

  ],
  scrollBehavior

})

export default router
