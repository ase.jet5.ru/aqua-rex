import axios from 'axios';

export const testLoc = 'https://aqua-rex.jet5.ru/api/';
export const locOrig = 'https://aqua-rex.ru';

export const HTTP = axios.create({
    baseURL: testLoc,
    timeout: 30000,
    // withCredentials: false,
    headers: {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
  }
  });

let pathLang = 'ru';
function langContent(){
  const path = window.location.pathname.split('/');
  // console.log('path: ', path);
  pathLang = path[1];
  // console.log('pathLangAPI: ', pathLang);
};

langContent();
export const Routes = {
  menu: `getmenuitems/?lang=${pathLang}`,
  productTypes: `getproductssections/?lang=${pathLang}`,
  productItems: `getproductsitembysection/?lang=${pathLang}`,
  homePage: `getpage/?lang=${pathLang}&page=home`,
  assort: `getPage/?lang=${pathLang}&page=template&`,
  about: `getpage/?lang=${pathLang}&page=about`,
  contacts: `getpage/?lang=${pathLang}&page=contacts`,
  news: `getpage/?lang=${pathLang}&page=template&news=Y`,
  newsyear: `getNewsItems/?lang=${pathLang}&`,
  newsItem: `getpage/?lang=${pathLang}&page=template&news=Y&`,
  postForm: 'saveFormData/',
  searchNews: `searchNews/?lang=${pathLang}`
  // err_page: `/includes/api/getData.php?type_data=err_page&lang=${pathLang}`,https://aqua-rex.jet5.ru/api/getpage/?lang=ru&page=template&news=Y
};
  // https://aqua-rex.jet5.ru/api/getmenuitems/?lang=ru