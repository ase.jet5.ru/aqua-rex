import './assets/main.css'

import { createApp } from 'vue'
import ymapPlugin from 'vue-yandex-maps';
import VueSmoothScroll from 'vue3-smooth-scroll'
import App from './App.vue'
import router from './router'
// import { createMetaManager} from 'vue-meta'


const app = createApp(App)
// const metaManager = createMetaManager()

app.use(router)
app.use(ymapPlugin)
app.use(VueSmoothScroll)
// app.use(metaManager)

app.mount('#app')

export function noScroll(isPopup){
    const div = document.createElement('div');
    div.style.overflowY = 'scroll';
    div.style.width = '50px';
    div.style.height = '50px';
    document.body.append(div);
    const scrollWidth = div.offsetWidth - div.clientWidth;
    // console.log('scrollWidth: ', scrollWidth);
    div.remove();
  
    document.body.style.overflow = isPopup? "hidden" : "auto";
    document.body.style.marginRight = isPopup? scrollWidth+'px' : 0;
  }