<?php

$arCustomPage = array (
  'PARENT_MENU' => 'global_menu_settings',
  'SORT' => '1780',
  'LANG' => 
  array (
    'ru' => 
    array (
      'MENU_TEXT' => 'Дополнительные настройки',
      'MENU_TITLE' => 'Дополнительные настройки',
      'PAGE_TITLE' => 'Дополнительные настройки',
    ),
    'en' => 
    array (
      'MENU_TEXT' => 'Additional settings',
      'MENU_TITLE' => 'Additional settings',
      'PAGE_TITLE' => 'Additional settings',
    ),
  ),
);

$arCustomSettings = array (
  1 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'API',
        'TITLE' => 'API',
      ),
      'en' => 
      array (
        'NAME' => '',
        'TITLE' => '',
      ),
    ),
    'SORT' => '500',
    'FIELDS' => 
    array (
      0 => 
      array (
        'NAME' => 'CS_SERVER_URL',
        'SORT' => '10',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'URL сайта (без протокола): ',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '',
        'SIZE' => '50',
      ),
      1 => 
      array (
        'NAME' => 'CS_METHODS_CACHETIME',
        'SORT' => '20',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Время жизни кеша (в секундах, 0 - нет кеша) :',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '0',
        'SIZE' => '20',
      ),
    ),
  ),
);

?>